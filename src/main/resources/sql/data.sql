insert into ACT_ID_GROUP values ('admin', 1, '管理员', 'security-role');
insert into ACT_ID_GROUP values ('user', 1, '用户', 'security-role');
insert into ACT_ID_GROUP values ('leaderuser', 1, '部门领导', 'assignment');
insert into ACT_ID_GROUP values ('hr', 1, '人事', 'assignment');
insert into ACT_ID_GROUP values ('genManager', 1, '总经理', 'assignment');

insert into ACT_ID_USER values ('admin', 1, 'Admin', 'Str', 'joseph_zhangyong@163.com', '123456', '');
insert into ACT_ID_MEMBERSHIP values ('admin', 'user');
insert into ACT_ID_MEMBERSHIP values ('admin', 'admin');

insert into ACT_ID_USER values ('user', 1, 'Zhang', 'Yong', 'joseph_zhangyong@163.com', '123456', '');
insert into ACT_ID_MEMBERSHIP values ('user', 'admin');

insert into ACT_ID_USER values ('hruser', 1, 'Zhang', 'Yong', 'joseph_zhangyong@163.com', '123456', '');
insert into ACT_ID_MEMBERSHIP values ('hruser', 'user');
insert into ACT_ID_MEMBERSHIP values ('hruser', 'hr');

insert into ACT_ID_USER values ('leaderuser', 1, 'Zhang', 'Yong', 'joseph_zhangyong@163.com', '123456', '');
insert into ACT_ID_MEMBERSHIP values ('leaderuser', 'user');
insert into ACT_ID_MEMBERSHIP values ('leaderuser', 'leaderuser');

insert into ACT_ID_USER values ('genManager', 1, 'Zhang', 'Yong', 'joseph_zhangyong@163.com', '123456', '');
insert into ACT_ID_MEMBERSHIP values ('genManager', 'genManager');