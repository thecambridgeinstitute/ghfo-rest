package com.ciie.ghfo.utils;

import java.io.File;

public class Util {
	
	public static File[] list(){
		String basePath=Util.class.getResource("/").getPath();
		basePath=basePath.substring(1,basePath.length());
		return new File(basePath+File.separator+"flowfile").listFiles();
	}

}
