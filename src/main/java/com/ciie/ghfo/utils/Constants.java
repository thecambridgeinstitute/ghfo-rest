package com.ciie.ghfo.utils;

/**
 * 
 * @author Administrator
 *
 */
public class Constants {
	
	public static final String USERNAME_KEY = "username";
	
	public static final String PASSWORD_KEY = "password";
	
	public static final String SUCCESS = "00000";
	
	public static final String FAIL = "999999";
	
}
