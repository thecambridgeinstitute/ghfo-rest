package com.ciie.ghfo.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.ciie.ghfo.commons.entity.AbstractEntity;

@Entity
@Table(name="hfob_member")
public class MemberEntity extends AbstractEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	private long id;

	@Column(length=45)
	private String name;
	
	@Column(length=45)
	private String relationship;
	
	private Date dateBirth;
	
	@Column(name="primary_language",length=45)
	private String primaryLanguage;
	
	@Column(name="secondary_language",length=45)
	private String secondaryLanguage;
	
	@Column(name="employment_industry", length=45)
	private String employmentIndustry;
	
	@Column(name="job_title",length=60)
	private String jobTitle;
	
	@Column(name="highest_education_Level",length=45)
	private String highestEducationLevel;
	
	@ManyToOne(cascade={CascadeType.ALL})
	@JoinColumn(name="app_id")
	private AppEntity appEntity;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRelationship() {
		return relationship;
	}

	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}

	public Date getDateBirth() {
		return dateBirth;
	}

	public void setDateBirth(Date dateBirth) {
		this.dateBirth = dateBirth;
	}

	public String getPrimaryLanguage() {
		return primaryLanguage;
	}

	public void setPrimaryLanguage(String primaryLanguage) {
		this.primaryLanguage = primaryLanguage;
	}

	public String getSecondaryLanguage() {
		return secondaryLanguage;
	}

	public void setSecondaryLanguage(String secondaryLanguage) {
		this.secondaryLanguage = secondaryLanguage;
	}

	public String getEmploymentIndustry() {
		return employmentIndustry;
	}

	public void setEmploymentIndustry(String employmentIndustry) {
		this.employmentIndustry = employmentIndustry;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public String getHighestEducationLevel() {
		return highestEducationLevel;
	}

	public void setHighestEducationLevel(String highestEducationLevel) {
		this.highestEducationLevel = highestEducationLevel;
	}

	public AppEntity getAppEntity() {
		return appEntity;
	}

	public void setAppEntity(AppEntity appEntity) {
		this.appEntity = appEntity;
	}

	@Override
	public String toString() {
		return "MemberEntity [id=" + id + ", name=" + name + ", relationship="
				+ relationship + ", dateBirth=" + dateBirth
				+ ", primaryLanguage=" + primaryLanguage
				+ ", secondaryLanguage=" + secondaryLanguage
				+ ", employmentIndustry=" + employmentIndustry + ", jobTitle="
				+ jobTitle + ", highestEducationLevel=" + highestEducationLevel
				+ ", appEntity=" + appEntity + "]";
	}
	
}
