package com.ciie.ghfo.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.ciie.ghfo.commons.entity.AbstractEntity;

/**
 *  App 实体类
 * @author Administrator
 *
 */
@Entity
@Table(name="hfob_app")
public class AppEntity extends AbstractEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 306147931752750986L;

	@Id
	private long id;
	
	private Boolean meals;
	
	@Column(name="own_bed")
	private Boolean ownBed;
	
	private Boolean transportation;
	
	@Column(name="home_type",length=80)
	private String homeType;
	
	@Column(name="no_family_member",length=45)
	private String noFamilyMember;
	
	@Column(name="household_avg_income", length=20)
	private String householdAvgIncome;
	
	@Column(name="work_schedule",length=200)
	private String workSchedule;
	
	@Column(name="have_pet")
	private Boolean havePet;
	
	@Column(name="medical_concern")
	private Boolean medicalConcern;
	
	private Boolean smoking;
	
	@Column(name="need_base_gov_sub")
	private Boolean needBaseGovSub;
	
	@Column(name="no_student",length=2)
	private Integer noStudent;
	
	@Column(name="previous_experience",length=200)
	private String previousExperience;
	
	@Column(name="why_interested",length=200)
	private String whyInterested;
	
	@Column(name="dietary_practice",length=100)
	private String dietaryPractice;
	
	@Column(name="dietary_flexble")
	private Boolean dietaryFlexble;
	
	@Column(length=100)
	private String hobby;
	
	@Column(name="family_type",length=45)
	private String familyType;
	
	@Column(name="religious_practice",length=100)
	private String religiousPractice;
	
	@Column(name="food_cooked",length=200)
	private String foodCooked;
	
	@Column(name="rules_expectation",length=200)
	private String rulesExpectation;
	
	@ManyToOne(cascade={CascadeType.ALL})
	@JoinColumn(name="profile_Id")
	private ProFileEntity proFileEntity;
	
	@OneToMany(mappedBy = "appEntity",cascade={CascadeType.ALL})
	private Set<SiteVisitEnity> siteVisits = new HashSet<SiteVisitEnity>();

	@OneToMany(mappedBy = "appEntity",cascade={CascadeType.ALL})
	private Set<MemberEntity> members = new HashSet<MemberEntity>();
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Boolean getMeals() {
		return meals;
	}

	public void setMeals(Boolean meals) {
		this.meals = meals;
	}

	public Boolean getOwnBed() {
		return ownBed;
	}

	public void setOwnBed(Boolean ownBed) {
		this.ownBed = ownBed;
	}

	public Boolean getTransportation() {
		return transportation;
	}

	public void setTransportation(Boolean transportation) {
		this.transportation = transportation;
	}

	public String getHomeType() {
		return homeType;
	}

	public void setHomeType(String homeType) {
		this.homeType = homeType;
	}

	public String getNoFamilyMember() {
		return noFamilyMember;
	}

	public void setNoFamilyMember(String noFamilyMember) {
		this.noFamilyMember = noFamilyMember;
	}

	public String getHouseholdAvgIncome() {
		return householdAvgIncome;
	}

	public void setHouseholdAvgIncome(String householdAvgIncome) {
		this.householdAvgIncome = householdAvgIncome;
	}

	public String getWorkSchedule() {
		return workSchedule;
	}

	public void setWorkSchedule(String workSchedule) {
		this.workSchedule = workSchedule;
	}

	public Boolean getHavePet() {
		return havePet;
	}

	public void setHavePet(Boolean havePet) {
		this.havePet = havePet;
	}

	public Boolean getMedicalConcern() {
		return medicalConcern;
	}

	public void setMedicalConcern(Boolean medicalConcern) {
		this.medicalConcern = medicalConcern;
	}

	public Boolean getSmoking() {
		return smoking;
	}

	public void setSmoking(Boolean smoking) {
		this.smoking = smoking;
	}

	public Boolean getNeedBaseGovSub() {
		return needBaseGovSub;
	}

	public void setNeedBaseGovSub(Boolean needBaseGovSub) {
		this.needBaseGovSub = needBaseGovSub;
	}

	public Integer getNoStudent() {
		return noStudent;
	}

	public void setNoStudent(Integer noStudent) {
		this.noStudent = noStudent;
	}

	public String getPreviousExperience() {
		return previousExperience;
	}

	public void setPreviousExperience(String previousExperience) {
		this.previousExperience = previousExperience;
	}

	public String getWhyInterested() {
		return whyInterested;
	}

	public void setWhyInterested(String whyInterested) {
		this.whyInterested = whyInterested;
	}

	public String getDietaryPractice() {
		return dietaryPractice;
	}

	public void setDietaryPractice(String dietaryPractice) {
		this.dietaryPractice = dietaryPractice;
	}

	public Boolean getDietaryFlexble() {
		return dietaryFlexble;
	}

	public void setDietaryFlexble(Boolean dietaryFlexble) {
		this.dietaryFlexble = dietaryFlexble;
	}

	public String getHobby() {
		return hobby;
	}

	public void setHobby(String hobby) {
		this.hobby = hobby;
	}

	public String getFamilyType() {
		return familyType;
	}

	public void setFamilyType(String familyType) {
		this.familyType = familyType;
	}

	public String getReligiousPractice() {
		return religiousPractice;
	}

	public void setReligiousPractice(String religiousPractice) {
		this.religiousPractice = religiousPractice;
	}

	public String getFoodCooked() {
		return foodCooked;
	}

	public void setFoodCooked(String foodCooked) {
		this.foodCooked = foodCooked;
	}

	public String getRulesExpectation() {
		return rulesExpectation;
	}

	public void setRulesExpectation(String rulesExpectation) {
		this.rulesExpectation = rulesExpectation;
	}

	public ProFileEntity getProFileEntity() {
		return proFileEntity;
	}

	public void setProFileEntity(ProFileEntity proFileEntity) {
		this.proFileEntity = proFileEntity;
	}

	public Set<SiteVisitEnity> getSiteVisits() {
		return siteVisits;
	}

	public void setSiteVisits(Set<SiteVisitEnity> siteVisits) {
		this.siteVisits = siteVisits;
	}

	public Set<MemberEntity> getMembers() {
		return members;
	}

	public void setMembers(Set<MemberEntity> members) {
		this.members = members;
	}

	@Override
	public String toString() {
		return "AppEntity [id=" + id + ", meals=" + meals + ", ownBed="
				+ ownBed + ", transportation=" + transportation + ", homeType="
				+ homeType + ", noFamilyMember=" + noFamilyMember
				+ ", householdAvgIncome=" + householdAvgIncome
				+ ", workSchedule=" + workSchedule + ", havePet=" + havePet
				+ ", medicalConcern=" + medicalConcern + ", smoking=" + smoking
				+ ", needBaseGovSub=" + needBaseGovSub + ", noStudent="
				+ noStudent + ", previousExperience=" + previousExperience
				+ ", whyInterested=" + whyInterested + ", dietaryPractice="
				+ dietaryPractice + ", dietaryFlexble=" + dietaryFlexble
				+ ", hobby=" + hobby + ", familyType=" + familyType
				+ ", religiousPractice=" + religiousPractice + ", foodCooked="
				+ foodCooked + ", rulesExpectation=" + rulesExpectation
				+ ", proFileEntity=" + proFileEntity + ", siteVisits="
				+ siteVisits + ", members=" + members + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((dietaryFlexble == null) ? 0 : dietaryFlexble.hashCode());
		result = prime * result
				+ ((dietaryPractice == null) ? 0 : dietaryPractice.hashCode());
		result = prime * result
				+ ((familyType == null) ? 0 : familyType.hashCode());
		result = prime * result
				+ ((foodCooked == null) ? 0 : foodCooked.hashCode());
		result = prime * result + ((havePet == null) ? 0 : havePet.hashCode());
		result = prime * result + ((hobby == null) ? 0 : hobby.hashCode());
		result = prime * result
				+ ((homeType == null) ? 0 : homeType.hashCode());
		result = prime
				* result
				+ ((householdAvgIncome == null) ? 0 : householdAvgIncome
						.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((meals == null) ? 0 : meals.hashCode());
		result = prime * result
				+ ((medicalConcern == null) ? 0 : medicalConcern.hashCode());
		result = prime * result + ((members == null) ? 0 : members.hashCode());
		result = prime * result
				+ ((needBaseGovSub == null) ? 0 : needBaseGovSub.hashCode());
		result = prime * result
				+ ((noFamilyMember == null) ? 0 : noFamilyMember.hashCode());
		result = prime * result
				+ ((noStudent == null) ? 0 : noStudent.hashCode());
		result = prime * result + ((ownBed == null) ? 0 : ownBed.hashCode());
		result = prime
				* result
				+ ((previousExperience == null) ? 0 : previousExperience
						.hashCode());
		result = prime * result
				+ ((proFileEntity == null) ? 0 : proFileEntity.hashCode());
		result = prime
				* result
				+ ((religiousPractice == null) ? 0 : religiousPractice
						.hashCode());
		result = prime
				* result
				+ ((rulesExpectation == null) ? 0 : rulesExpectation.hashCode());
		result = prime * result
				+ ((siteVisits == null) ? 0 : siteVisits.hashCode());
		result = prime * result + ((smoking == null) ? 0 : smoking.hashCode());
		result = prime * result
				+ ((transportation == null) ? 0 : transportation.hashCode());
		result = prime * result
				+ ((whyInterested == null) ? 0 : whyInterested.hashCode());
		result = prime * result
				+ ((workSchedule == null) ? 0 : workSchedule.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AppEntity other = (AppEntity) obj;
		if (dietaryFlexble == null) {
			if (other.dietaryFlexble != null)
				return false;
		} else if (!dietaryFlexble.equals(other.dietaryFlexble))
			return false;
		if (dietaryPractice == null) {
			if (other.dietaryPractice != null)
				return false;
		} else if (!dietaryPractice.equals(other.dietaryPractice))
			return false;
		if (familyType == null) {
			if (other.familyType != null)
				return false;
		} else if (!familyType.equals(other.familyType))
			return false;
		if (foodCooked == null) {
			if (other.foodCooked != null)
				return false;
		} else if (!foodCooked.equals(other.foodCooked))
			return false;
		if (havePet == null) {
			if (other.havePet != null)
				return false;
		} else if (!havePet.equals(other.havePet))
			return false;
		if (hobby == null) {
			if (other.hobby != null)
				return false;
		} else if (!hobby.equals(other.hobby))
			return false;
		if (homeType == null) {
			if (other.homeType != null)
				return false;
		} else if (!homeType.equals(other.homeType))
			return false;
		if (householdAvgIncome == null) {
			if (other.householdAvgIncome != null)
				return false;
		} else if (!householdAvgIncome.equals(other.householdAvgIncome))
			return false;
		if (id != other.id)
			return false;
		if (meals == null) {
			if (other.meals != null)
				return false;
		} else if (!meals.equals(other.meals))
			return false;
		if (medicalConcern == null) {
			if (other.medicalConcern != null)
				return false;
		} else if (!medicalConcern.equals(other.medicalConcern))
			return false;
		if (members == null) {
			if (other.members != null)
				return false;
		} else if (!members.equals(other.members))
			return false;
		if (needBaseGovSub == null) {
			if (other.needBaseGovSub != null)
				return false;
		} else if (!needBaseGovSub.equals(other.needBaseGovSub))
			return false;
		if (noFamilyMember == null) {
			if (other.noFamilyMember != null)
				return false;
		} else if (!noFamilyMember.equals(other.noFamilyMember))
			return false;
		if (noStudent == null) {
			if (other.noStudent != null)
				return false;
		} else if (!noStudent.equals(other.noStudent))
			return false;
		if (ownBed == null) {
			if (other.ownBed != null)
				return false;
		} else if (!ownBed.equals(other.ownBed))
			return false;
		if (previousExperience == null) {
			if (other.previousExperience != null)
				return false;
		} else if (!previousExperience.equals(other.previousExperience))
			return false;
		if (proFileEntity == null) {
			if (other.proFileEntity != null)
				return false;
		} else if (!proFileEntity.equals(other.proFileEntity))
			return false;
		if (religiousPractice == null) {
			if (other.religiousPractice != null)
				return false;
		} else if (!religiousPractice.equals(other.religiousPractice))
			return false;
		if (rulesExpectation == null) {
			if (other.rulesExpectation != null)
				return false;
		} else if (!rulesExpectation.equals(other.rulesExpectation))
			return false;
		if (siteVisits == null) {
			if (other.siteVisits != null)
				return false;
		} else if (!siteVisits.equals(other.siteVisits))
			return false;
		if (smoking == null) {
			if (other.smoking != null)
				return false;
		} else if (!smoking.equals(other.smoking))
			return false;
		if (transportation == null) {
			if (other.transportation != null)
				return false;
		} else if (!transportation.equals(other.transportation))
			return false;
		if (whyInterested == null) {
			if (other.whyInterested != null)
				return false;
		} else if (!whyInterested.equals(other.whyInterested))
			return false;
		if (workSchedule == null) {
			if (other.workSchedule != null)
				return false;
		} else if (!workSchedule.equals(other.workSchedule))
			return false;
		return true;
	}
	
}
