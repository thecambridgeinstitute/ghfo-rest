package com.ciie.ghfo.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.ciie.ghfo.commons.entity.AbstractEntity;


@Entity
@Table(name="hfob_site_visit")
public class SiteVisitEnity extends AbstractEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1910971828292192683L;
	
	@Id
	private long id;
	
	@Column(name="house_type",length=45)
	private String houseType;
	
	@Column(name="square_footage",length=45)
	private String squareFootage;
	
	@Column(name="no_bedroom",length=2)
	private Integer noBedroom;
	
	@Column(name="no_bathroom",length=2)
	private Integer nobathroom;
	
	@Column(name="where_student_sleep",length=100)
	private String whereStudentSleep;
	
	@Column(name="distance_from_school",length=45)
	private String distanceFromSchool;
	
	@Column(name="amenities_accessible",length=200)
	private String amenitiesAccessible;

	@ManyToOne(cascade={CascadeType.ALL})
	@JoinColumn(name="app_id")
	private AppEntity appEntity;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getHouseType() {
		return houseType;
	}

	public void setHouseType(String houseType) {
		this.houseType = houseType;
	}

	public String getSquareFootage() {
		return squareFootage;
	}

	public void setSquareFootage(String squareFootage) {
		this.squareFootage = squareFootage;
	}

	public Integer getNoBedroom() {
		return noBedroom;
	}

	public void setNoBedroom(Integer noBedroom) {
		this.noBedroom = noBedroom;
	}

	public Integer getNobathroom() {
		return nobathroom;
	}

	public void setNobathroom(Integer nobathroom) {
		this.nobathroom = nobathroom;
	}

	public String getWhereStudentSleep() {
		return whereStudentSleep;
	}

	public void setWhereStudentSleep(String whereStudentSleep) {
		this.whereStudentSleep = whereStudentSleep;
	}

	public String getDistanceFromSchool() {
		return distanceFromSchool;
	}

	public void setDistanceFromSchool(String distanceFromSchool) {
		this.distanceFromSchool = distanceFromSchool;
	}

	public String getAmenitiesAccessible() {
		return amenitiesAccessible;
	}

	public void setAmenitiesAccessible(String amenitiesAccessible) {
		this.amenitiesAccessible = amenitiesAccessible;
	}

	public AppEntity getAppEntity() {
		return appEntity;
	}

	public void setAppEntity(AppEntity appEntity) {
		this.appEntity = appEntity;
	}

	@Override
	public String toString() {
		return "SiteVisitEnity [id=" + id + ", houseType=" + houseType
				+ ", squareFootage=" + squareFootage + ", noBedroom="
				+ noBedroom + ", nobathroom=" + nobathroom
				+ ", whereStudentSleep=" + whereStudentSleep
				+ ", distanceFromSchool=" + distanceFromSchool
				+ ", amenitiesAccessible=" + amenitiesAccessible
				+ ", appEntity=" + appEntity + "]";
	}
	
}
