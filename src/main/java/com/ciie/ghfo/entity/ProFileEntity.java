package com.ciie.ghfo.entity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.ciie.ghfo.commons.entity.AbstractEntity;
import com.ciie.ghfo.rest.api.profile.form.ProfileForm;

/**
 * profile实体类
 * @author Administrator
 *
 */
@Entity
@Table(name="hfob_profile")
public class ProFileEntity extends AbstractEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	private long id;
	
	@Column(name="first_name",length=45)
	private String fName;
	
	@Column(name="last_name",length=45)
	private String lName;
	
	@Column(name="date_birth")
	private Date dateBirth;
	
	@Column(length=10)
	private String gender;
	
	private Date applicationDate;
	
	@Column(length=14)
	private String phone;
	
	@Column(length=60)
	private String eMail;
	
	@Column(length=80)
	private String street;
	
	@Column(length=30)
	private String city;
	
	@Column(length=2)
	private String state;
	
	@Column(length=5)
	private Integer zipCode;
	
	@Column(length=80)
	private String source;
	
	@OneToMany(mappedBy = "proFileEntity",cascade={CascadeType.ALL})
	private Set<AppEntity> apps = new HashSet<AppEntity>();

	@Column(length=30)
	private String processInstanceId;
	
	public ProFileEntity(ProfileForm profileForm){
		fName = profileForm.getfName();
		lName = profileForm.getlName();
		dateBirth = profileForm.getDateBirth();
		gender = profileForm.getGender();
		applicationDate = profileForm.getApplicationDate();
		phone = profileForm.getPhone();
		eMail = profileForm.geteMail();
		street = profileForm.getStreet();
		city = profileForm.getCity();
		state = profileForm.getState();
		zipCode = profileForm.getZipCode();
		source = profileForm.getSource();
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public ProFileEntity(String fName, String lName){
		this.fName = fName;
		this.lName = lName;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getfName() {
		return fName;
	}

	public void setfName(String fName) {
		this.fName = fName;
	}

	public String getlName() {
		return lName;
	}

	public void setlName(String lName) {
		this.lName = lName;
	}

	public Date getDateBirth() {
		return dateBirth;
	}

	public void setDateBirth(Date dateBirth) {
		this.dateBirth = dateBirth;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Date getApplicationDate() {
		return applicationDate;
	}

	public void setApplicationDate(Date applicationDate) {
		this.applicationDate = applicationDate;
	}

	public String geteMail() {
		return eMail;
	}

	public void seteMail(String eMail) {
		this.eMail = eMail;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Integer getZipCode() {
		return zipCode;
	}

	public void setZipCode(Integer zipCode) {
		this.zipCode = zipCode;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public Set<AppEntity> getApps() {
		return apps;
	}

	public void setApps(Set<AppEntity> apps) {
		this.apps = apps;
	}

	@Override
	public String toString() {
		return "ProFileEntity [id=" + id + ", fName=" + fName + ", lName="
				+ lName + ", dateBirth=" + dateBirth + ", gender=" + gender
				+ ", applicationDate=" + applicationDate + ", phone=" + phone
				+ ", eMail=" + eMail + ", street=" + street + ", city=" + city
				+ ", state=" + state + ", zipCode=" + zipCode + ", source="
				+ source + ", apps=" + apps + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((applicationDate == null) ? 0 : applicationDate.hashCode());
		result = prime * result + ((apps == null) ? 0 : apps.hashCode());
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result
				+ ((dateBirth == null) ? 0 : dateBirth.hashCode());
		result = prime * result + ((eMail == null) ? 0 : eMail.hashCode());
		result = prime * result + ((fName == null) ? 0 : fName.hashCode());
		result = prime * result + ((gender == null) ? 0 : gender.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((lName == null) ? 0 : lName.hashCode());
		result = prime * result + ((phone == null) ? 0 : phone.hashCode());
		result = prime * result + ((source == null) ? 0 : source.hashCode());
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		result = prime * result + ((street == null) ? 0 : street.hashCode());
		result = prime * result + ((zipCode == null) ? 0 : zipCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProFileEntity other = (ProFileEntity) obj;
		if (applicationDate == null) {
			if (other.applicationDate != null)
				return false;
		} else if (!applicationDate.equals(other.applicationDate))
			return false;
		if (apps == null) {
			if (other.apps != null)
				return false;
		} else if (!apps.equals(other.apps))
			return false;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (dateBirth == null) {
			if (other.dateBirth != null)
				return false;
		} else if (!dateBirth.equals(other.dateBirth))
			return false;
		if (eMail == null) {
			if (other.eMail != null)
				return false;
		} else if (!eMail.equals(other.eMail))
			return false;
		if (fName == null) {
			if (other.fName != null)
				return false;
		} else if (!fName.equals(other.fName))
			return false;
		if (gender == null) {
			if (other.gender != null)
				return false;
		} else if (!gender.equals(other.gender))
			return false;
		if (id != other.id)
			return false;
		if (lName == null) {
			if (other.lName != null)
				return false;
		} else if (!lName.equals(other.lName))
			return false;
		if (phone == null) {
			if (other.phone != null)
				return false;
		} else if (!phone.equals(other.phone))
			return false;
		if (source == null) {
			if (other.source != null)
				return false;
		} else if (!source.equals(other.source))
			return false;
		if (state == null) {
			if (other.state != null)
				return false;
		} else if (!state.equals(other.state))
			return false;
		if (street == null) {
			if (other.street != null)
				return false;
		} else if (!street.equals(other.street))
			return false;
		if (zipCode == null) {
			if (other.zipCode != null)
				return false;
		} else if (!zipCode.equals(other.zipCode))
			return false;
		return true;
	}
	
}
