package com.ciie.ghfo.init;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import javax.annotation.Resource;

import org.activiti.engine.RepositoryService;
import org.apache.log4j.Logger;

import com.ciie.ghfo.utils.Util;

/**
 * 系统初始化方法
 * @author Administrator
 */
public class InitService {
	
	Logger logger = Logger.getLogger(InitService.class);
	
	@Resource
	private RepositoryService repositoryService;
	
	public void init(){
		logger.info("Init start.....");
		File[] files = Util.list();
		InputStream fileInputStream = null;
		try {
			for (File file : files) {
				if(file.getName().equals("leave.bpmn")){
					fileInputStream = new FileInputStream(file);
					repositoryService.createDeployment().addInputStream(file.getName(), fileInputStream).deploy();
				}
			}
			logger.info("Init end.....");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			logger.info("Init fail:"+e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Init fail:"+e.getMessage());
		}
	}
}
