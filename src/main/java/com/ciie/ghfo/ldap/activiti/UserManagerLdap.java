package com.ciie.ghfo.ldap.activiti;

import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.InitialDirContext;

import org.activiti.engine.identity.User;
import org.activiti.ldap.LDAPCallBack;
import org.activiti.ldap.LDAPConfigurator;
import org.activiti.ldap.LDAPTemplate;
import org.activiti.ldap.LDAPUserManager;

public class UserManagerLdap extends LDAPUserManager {

	public UserManagerLdap(LDAPConfigurator ldapConfigurator) {
		super(ldapConfigurator);
	}
	
	@Override
	public void insertUser(final User user) {
		LDAPTemplate ldapTemplate = new LDAPTemplate(ldapConfigurator);
		ldapTemplate.execute(new LDAPCallBack<Boolean>() {

			@Override
			public Boolean executeInContext(InitialDirContext initialDirContext) {
				if(initialDirContext == null){
					return false;
				}
				Attributes attrs = new BasicAttributes();
				attrs.put("objectclass", "top,person,organizationalPerson,inetorgperson");
				attrs.put("uid", user.getId());
				attrs.put("cn", user.getFirstName());
				attrs.put("mail", user.getEmail());
				attrs.put("sn", user.getLastName());
				
				try {
					initialDirContext.createSubcontext(user.getId(), attrs);
					return true;
				} catch (NamingException e) {
					e.printStackTrace();
					return false;
				}
				
			}
		});
	}
	@Override
	public void deleteUser(final String userDn) {
		LDAPTemplate ldapTemplate = new LDAPTemplate(ldapConfigurator);
		ldapTemplate.execute(new LDAPCallBack<Boolean>() {

			@Override
			public Boolean executeInContext(InitialDirContext initialDirContext) {
				if(initialDirContext == null){
					return false;
				}
				try {
					initialDirContext.destroySubcontext(userDn);
					return true;
				} catch (NamingException e) {
					return false;
				}
			}
			
		});
	}
}
