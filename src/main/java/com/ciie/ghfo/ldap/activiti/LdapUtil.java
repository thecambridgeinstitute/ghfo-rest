package com.ciie.ghfo.ldap.activiti;

import java.util.List;

import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.InitialDirContext;

import org.activiti.engine.identity.User;
import org.activiti.engine.impl.UserQueryImpl;
import org.activiti.engine.impl.persistence.entity.UserEntity;
import org.activiti.ldap.LDAPConfigurator;
import org.activiti.ldap.LDAPConnectionUtil;
import org.activiti.ldap.LDAPUserManager;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.ciie.ghfo.commons.utils.SpringContextUtil;

public class LdapUtil {
	
	public static boolean validate(String DN){
		boolean result = false;
        
        LDAPConfigurator configurator = (LDAPConfigurator)SpringContextUtil.getBean("LDAPConfigurator");
          
//        LDAPConfigurator configurator = (LDAPConfigurator) beanFactory.getBean("LDAPConfigurator");  
        InitialDirContext ctx = LDAPConnectionUtil.creatDirectoryContext(configurator); 
		
        try {
			Attributes attrs = ctx.getAttributes(DN);
			if(attrs != null){
				result = true;
			}
			
		} catch (NamingException e) {
			result = false;
			e.printStackTrace();
		}
		return result;
	}
	public static void main(String[] args) {
		String path = "activiti.cfg.xml";  
        Resource resource = new ClassPathResource(path);  
        BeanFactory beanFactory = new XmlBeanFactory(resource); 
          
        LDAPConfigurator ldapConfigurator = (LDAPConfigurator) beanFactory.getBean("LDAPConfigurator"); 
		UserManagerLdap manager = new UserManagerLdap(ldapConfigurator);
		UserEntity user = new UserEntity();
		user.setId("ychao");
		user.setEmail("123@qq.com");
		user.setFirstName("Yu Chao");
		user.setLastName("Chao");
		manager.insertUser(user);
		System.out.println("//////////////////////////");
		
	}
}
