package com.ciie.ghfo.commons.utils;

import java.util.List;
import java.util.Map;

/**
 * 验证工具类
 * 
 */
public class VerifyUtil {
	/**
	 * <p>
	 * 不为空验证
	 * </p>
	 * 
	 * @param obj
	 * @return
	 */
	public static boolean isNotNull(Object obj) {
		return null != obj;
	}

	/**
	 * <p>
	 * 判断List不为空
	 * </p>
	 * 
	 * @param list
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public static boolean listNotEmpty(List list) {
		return isNotNull(list) && list.size() > 0;
	}

	/**
	 * 验证字符串
	 */
	public static boolean stringNotEmpty(String string) {
		return isNotNull(string) && !string.equals("");
	}

	/**
	 * 验证map不为空
	 * 
	 * @param map
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public static boolean mapNotEmpty(Map map) {
		return isNotNull(map) && map.size() > 0;
	}
}
