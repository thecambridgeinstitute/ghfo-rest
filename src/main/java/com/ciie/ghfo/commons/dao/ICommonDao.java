package com.ciie.ghfo.commons.dao;

import java.io.Serializable;
import java.util.List;

import com.ciie.ghfo.commons.entity.AbstractEntity;

/**
 * 
 * DAO接口基类
 * @author zhangyong
 *
 */
public interface ICommonDao {

	public <T extends AbstractEntity> T save(T model);

	public <T extends AbstractEntity> void saveOrUpdate(T model);

	public <T extends AbstractEntity> void update(T model);

	public <T extends AbstractEntity> void merge(T model);

	public <T extends AbstractEntity, PK extends Serializable> void delete(
			Class<T> entityClass, PK id);

	public <T extends AbstractEntity> void deleteObject(T model);

	public <T extends AbstractEntity, PK extends Serializable> T get(
			Class<T> entityClass, PK id);

	public <T extends AbstractEntity> Long countAll(Class<T> entityClass);
	
	public <T extends AbstractEntity> List<T> listAll(Class<T> entityClass);

}
