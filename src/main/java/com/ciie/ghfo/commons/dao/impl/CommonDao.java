package com.ciie.ghfo.commons.dao.impl;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.ciie.ghfo.commons.dao.ICommonDao;
import com.ciie.ghfo.commons.entity.AbstractEntity;

@Component("CommonDao")
public class CommonDao implements ICommonDao {

	@Autowired
	@Qualifier("sessionFactory")
	private SessionFactory sessionFactory;

	public Session getSession() {
		return sessionFactory.openSession();
	}

	public Query getQuery(String hql) {
		Query query = getSession().createQuery(hql);
		query.setCacheable(true);
		return query;
	}

	public <T extends AbstractEntity> Criteria getCriteria(Class<T> entityClass) {
		return getSession().createCriteria(entityClass);
	}

	@Override
	public <T extends AbstractEntity> T save(T model) {
		getSession().save(model);
		return model;
	}

	@Override
	public <T extends AbstractEntity> void saveOrUpdate(T model) {
		getSession().saveOrUpdate(model);

	}

	@Override
	public <T extends AbstractEntity> void update(T model) {
		getSession().update(model);
	}

	@Override
	public <T extends AbstractEntity> void merge(T model) {
		getSession().merge(model);
	}

	@Override
	public <T extends AbstractEntity, PK extends Serializable> void delete(
			Class<T> entityClass, PK id) {
		getSession().delete(get(entityClass, id));
	}

	@Override
	public <T extends AbstractEntity> void deleteObject(T model) {
		getSession().delete(model);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T extends AbstractEntity, PK extends Serializable> T get(
			Class<T> entityClass, PK id) {
		return (T) getSession().get(entityClass, id);
	}

	@Override
	public <T extends AbstractEntity> Long countAll(Class<T> entityClass) {
		Criteria criteria = getSession().createCriteria(entityClass);
		criteria.setProjection(Projections.rowCount());
		return ((Long) criteria.uniqueResult()).longValue();
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T extends AbstractEntity> List<T> listAll(Class<T> entityClass) {
		Criteria criteria = getSession().createCriteria(entityClass);
		return criteria.list();
	}
}
