package com.ciie.ghfo.commons.entity;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.ciie.ghfo.commons.services.ICommonService;
import com.ciie.ghfo.commons.utils.SpringContextUtil;

/**
 * 实体抽象类
 * @author Administrator
 *
 */
public class AbstractEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
    
    public void save() {
        ICommonService commonService = SpringContextUtil.getBean("CommonService");
        commonService.save(this);
    }
    
    public void delete() {
        ICommonService commonService = SpringContextUtil.getBean("CommonService");
        commonService.deleteObject(this);
    }
    
    public void update() {
        ICommonService commonService = SpringContextUtil.getBean("CommonService");
        commonService.update(this);
    }

}
