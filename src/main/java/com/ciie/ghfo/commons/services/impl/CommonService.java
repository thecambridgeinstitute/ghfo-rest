package com.ciie.ghfo.commons.services.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.ciie.ghfo.commons.dao.ICommonDao;
import com.ciie.ghfo.commons.entity.AbstractEntity;
import com.ciie.ghfo.commons.services.ICommonService;

@Service("CommonService")
public class CommonService implements ICommonService {

	@Autowired
	@Qualifier("CommonDao")
	private ICommonDao commonDao;

	@Override
	public <T extends AbstractEntity> T save(T model) {
		return commonDao.save(model);
	}

	@Override
	public <T extends AbstractEntity> void saveOrUpdate(T model) {
		commonDao.saveOrUpdate(model);
	}

	@Override
	public <T extends AbstractEntity> void update(T model) {
		commonDao.update(model);
	}

	@Override
	public <T extends AbstractEntity> void merge(T model) {
		commonDao.merge(model);
	}

	@Override
	public <T extends AbstractEntity, PK extends Serializable> void delete(
			Class<T> entityClass, PK id) {
		commonDao.delete(entityClass, id);
	}

	@Override
	public <T extends AbstractEntity> void deleteObject(T model) {
		commonDao.deleteObject(model);
	}

	@Override
	public <T extends AbstractEntity, PK extends Serializable> T get(
			Class<T> entityClass, PK id) {
		return commonDao.get(entityClass, id);

	}

	@Override
	public <T extends AbstractEntity> Long countAll(Class<T> entityClass) {
		return commonDao.countAll(entityClass);
	}

	@Override
	public <T extends AbstractEntity> List<T> listAll(Class<T> entityClass) {
		return commonDao.listAll(entityClass);
	}
}
