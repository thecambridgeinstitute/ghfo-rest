package com.ciie.ghfo.services.impl;

import java.util.Map;

import org.activiti.engine.IdentityService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.runtime.ProcessInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ciie.ghfo.entity.ProFileEntity;
import com.ciie.ghfo.services.WorkflowService;

@Service("workflowService")
public class WorkflowServiceImpl implements WorkflowService {

	private static Logger logger = LoggerFactory.getLogger(WorkflowServiceImpl.class);
	
	@Autowired
    private IdentityService identityService;
	
	@Autowired
	private RuntimeService runtimeService;
	
	@Override
	public ProcessInstance startWorkflow(ProFileEntity proFileEntity,
			Map<String, Object> variables) {
		 long businessKey = proFileEntity.getId();
		 ProcessInstance processInstance = null;
		 try{
			 // 用来设置启动流程的人员ID，自动保存到activiti:initiator
			 identityService.setAuthenticatedUserId(proFileEntity.getfName()+" " +proFileEntity.getlName());
			 processInstance = runtimeService.startProcessInstanceByKey("GP_Process", businessKey+"", variables);
			 String processInstanceId = processInstance.getId();
			 proFileEntity.setProcessInstanceId(processInstanceId);
			 logger.info("start process of {key={}, bkey={}, pid={}, variables={}}", new Object[]{"GP_Process", businessKey, processInstanceId, variables});
		 }catch(Exception e){
			 logger.error("start process fail:"+e.getMessage());
		 }finally{
			 identityService.setAuthenticatedUserId(null);
		 }
		return processInstance;
	}

}
