package com.ciie.ghfo.services.impl;

import org.springframework.stereotype.Service;

import com.ciie.ghfo.commons.services.impl.CommonService;
import com.ciie.ghfo.services.ProfileService;

/**
 * profile 服务类
 * @author Administrator
 *
 */
@Service("profileServices")
public class ProfileServiceImpl extends CommonService implements ProfileService {

}
