package com.ciie.ghfo.services.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.ciie.ghfo.dao.UserDao;
import com.ciie.ghfo.rest.api.user.form.UserDetails;
import com.ciie.ghfo.services.UserService;

@Service("userService")
public class UserServiceImpl implements UserService {

	@Resource
	private UserDao userDao;
	
	@Override
	public List<UserDetails> getAllUser() {
		return userDao.getAllUser();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<UserDetails> findUserById(String userId) {
		return userDao.findUser(userId);
	}

}
