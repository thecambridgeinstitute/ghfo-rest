package com.ciie.ghfo.services;

import java.util.List;

import com.ciie.ghfo.rest.api.user.form.UserDetails;

public interface UserService {

	public List<UserDetails> getAllUser();
	
	public List<UserDetails> findUserById(String userId);
}
