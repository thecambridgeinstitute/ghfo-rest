package com.ciie.ghfo.services;

import java.util.Map;

import org.activiti.engine.runtime.ProcessInstance;

import com.ciie.ghfo.entity.ProFileEntity;

public interface WorkflowService {

	/**
	 * 启动流程
	 * @param proFileEntity
	 * @param variables
	 * @return
	 */
	public ProcessInstance startWorkflow(ProFileEntity proFileEntity,Map<String, Object> variables);
	
	
}
