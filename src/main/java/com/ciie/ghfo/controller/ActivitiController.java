package com.ciie.ghfo.controller;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;

import org.activiti.bpmn.converter.BpmnXMLConverter;
import org.activiti.bpmn.model.BpmnModel;
import org.activiti.editor.constants.ModelDataJsonConstants;
import org.activiti.editor.language.json.converter.BpmnJsonConverter;
import org.activiti.engine.IdentityService;
import org.activiti.engine.ManagementService;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.impl.cmd.GetDeploymentProcessDiagramCmd;
import org.activiti.engine.impl.interceptor.Command;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.Model;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

@Controller
@RequestMapping("/process")
public class ActivitiController {

	@Resource(name="processEngine")
	private ProcessEngine engine;
	
	@Resource
	private RepositoryService repositoryService;
	
	@Resource
	private RuntimeService runtimeService;
	
	@Resource
	private TaskService taskService;
	
	@Resource
	private ManagementService managementService;
	
	@Resource
	private IdentityService identityService;

	/**
	 * 列出所有流程模板
	 */
	@RequestMapping("modelList")
	public ModelAndView list(ModelAndView mav,HttpServletRequest request) {
		List<Model> list = repositoryService.createModelQuery().list();
		mav.addObject("list", list);
		mav.setViewName("process/template");
		return mav;
	}
	
	@RequestMapping("deleteModel")
	public ModelAndView deleteModel(ModelAndView mav,String modelId){
		repositoryService.deleteModel(modelId);
		mav.setViewName("redirect:/process");
		return mav;
	}

	/**
	 * 部署流程
	 */
	@RequestMapping("deploy")
	public ModelAndView deploy(@RequestParam("processName")String processName, ModelAndView mav) {
		if (null != processName){
			repositoryService.createProcessDefinitionQuery().deploymentId(processName).list();
		}
		mav.addObject("list", repositoryService.createProcessDefinitionQuery().list());
		mav.setViewName("process/deployed");
		return mav;
	}

	/**
	 * 已部署流程列表
	 */
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView deployed(ModelAndView mav) {
		List<ProcessDefinition> list = repositoryService.createProcessDefinitionQuery().list();
		mav.addObject("list", list);
		mav.setViewName("process/deployed");
		return mav;
	}

	@RequestMapping("convertToModel")
	public ModelAndView convertToModel(String id,ModelAndView mav){
		try {
			ProcessDefinition processDefinition = repositoryService
					.createProcessDefinitionQuery().processDefinitionId(id)
					.singleResult();
			InputStream bpmnStream = repositoryService.getResourceAsStream(
					processDefinition.getDeploymentId(),
					processDefinition.getResourceName());
			XMLInputFactory xif = XMLInputFactory.newInstance();
			InputStreamReader in = new InputStreamReader(bpmnStream, "UTF-8");
			XMLStreamReader xtr = xif.createXMLStreamReader(in);
			BpmnModel bpmnModel = new BpmnXMLConverter()
					.convertToBpmnModel(xtr);

			BpmnJsonConverter converter = new BpmnJsonConverter();
			com.fasterxml.jackson.databind.node.ObjectNode modelNode = converter
					.convertToJson(bpmnModel);
			Model modelData = repositoryService.newModel();
			modelData.setKey(processDefinition.getKey());
			modelData.setName(processDefinition.getResourceName());
			modelData.setCategory(processDefinition.getDeploymentId());

			ObjectNode modelObjectNode = new ObjectMapper().createObjectNode();
			modelObjectNode.put(ModelDataJsonConstants.MODEL_NAME,
					processDefinition.getName());
			modelObjectNode.put(ModelDataJsonConstants.MODEL_REVISION, 1);
			modelObjectNode.put(ModelDataJsonConstants.MODEL_DESCRIPTION,
					processDefinition.getDescription());
			modelData.setMetaInfo(modelObjectNode.toString());
			repositoryService.saveModel(modelData);
			repositoryService.addModelEditorSource(modelData.getId(), modelNode
					.toString().getBytes("utf-8"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		mav.setViewName("redirect:modelList");
		return mav;
	}
	
	@RequestMapping("deployModel")
	public ModelAndView deployModel(String modelId,ModelAndView mav, RedirectAttributes redirectAttributes){
		try {
			Model modelData = repositoryService.getModel(modelId);
	        ObjectNode modelNode;
			modelNode = (ObjectNode) new ObjectMapper().readTree(repositoryService.getModelEditorSource(modelData.getId()));
	        byte[] bpmnBytes = null;
	        BpmnModel model = new BpmnJsonConverter().convertToBpmnModel(modelNode);
	        bpmnBytes = new BpmnXMLConverter().convertToXML(model);
	        String processName = modelData.getName() + ".bpmn20.xml";
	        Deployment deployment = repositoryService.createDeployment().name(modelData.getName()).addString(processName, new String(bpmnBytes)).deploy();
	        redirectAttributes.addFlashAttribute("message", "部署成功，部署ID=" + deployment.getId());
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mav.setViewName("redirect:modelList");
		return mav;
	}
	
	/**
	 * 启动一个流程实例
	 */
	@RequestMapping("start")
	public ModelAndView start(String id, ModelAndView mav) {
		identityService.setAuthenticatedUserId("jzhang");
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("fName", "zhangyong");
		variables.put("email", "joseph.zhang@cssinco.cn");
		runtimeService.startProcessInstanceById(id,"11",variables);
		List<ProcessInstance> list = runtimeService.createProcessInstanceQuery().list();
		mav.addObject("list", list);
		mav.setViewName("process/started");

		return mav;
	}

	/**
	 * 所有已启动流程实例
	 */
	@RequestMapping("started")
	public ModelAndView started(ModelAndView mav) {

		List<ProcessInstance> list = runtimeService.createProcessInstanceQuery()
				.list();

		mav.addObject("list", list);
		mav.setViewName("process/started");
		return mav;
	}
	
	/**
	 * 任务列表
	 * @param mav
	 * @return
	 */
	@RequestMapping("task")
	public ModelAndView task(ModelAndView mav){
		TaskService service=engine.getTaskService();
		List<Task> list=service.createTaskQuery().list();
		mav.addObject("list", list);
		mav.setViewName("process/task");
		return mav;
	}
	
	/**
	 * 完成流程
	 * @param mav
	 * @param id
	 * @return
	 */
	@RequestMapping("complete")
	public ModelAndView complete(ModelAndView mav,String id){
		
		taskService.complete(id);
		
		return new ModelAndView("redirect:task");
	}

	/**
	 * 所有已启动流程实例
	 * 
	 * @throws IOException
	 */
	@RequestMapping("graphics")
	public void graphics(String definitionId, String instanceId,
			String taskId, ModelAndView mav, HttpServletResponse response)
			throws IOException {
		
		response.setContentType("image/png");
		Command<InputStream> cmd = null;

		if (definitionId != null) {
			cmd = new GetDeploymentProcessDiagramCmd(definitionId);
		}

		if (instanceId != null) {
			cmd = new ProcessInstanceDiagramCmd(instanceId);
		}

		if (taskId != null) {
			Task task = taskService.createTaskQuery().taskId(taskId).singleResult();
			cmd = new ProcessInstanceDiagramCmd(
					task.getProcessInstanceId());
		}

		if (cmd != null) {
			InputStream is = managementService.executeCommand(cmd);
			int len = 0;
			byte[] b = new byte[1024];
			while ((len = is.read(b, 0, 1024)) != -1) {
				response.getOutputStream().write(b, 0, len);
			}
		}
	}
	
	/**
	 * 取消一个已部署的流程
	 * @param id
	 * @param mav
	 * @return
	 */
	@RequestMapping("distory")
	public ModelAndView distory(String id, ModelAndView mav){
		repositoryService.deleteDeployment(id, true);//true代表联级删除子任务，应用时考虑性能
		return new ModelAndView("redirect:/process");
	}
}
