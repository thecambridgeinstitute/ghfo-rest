package com.ciie.ghfo.controller;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jodd.util.Base64;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.ciie.ghfo.services.ProfileService;
import com.ciie.ghfo.utils.Constants;

@Controller
@RequestMapping("/login")
public class LoginController{

	@Autowired
	private ProfileService profileService;
	
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView goLogin(ModelAndView mav){
		mav.setViewName("login/login");
		return mav;
	}
	
	@RequestMapping("gologin")
	public ModelAndView login(ModelAndView mav ,HttpServletRequest request, HttpServletResponse response,
			String username,String password){
		String base64Code = "Basic " + Base64.encodeToString(username+":"+password);
		request.getSession().setAttribute(Constants.USERNAME_KEY, username);
		request.getSession().setAttribute(Constants.PASSWORD_KEY, password);
		request.getSession().setAttribute("BASE_64_CODE", base64Code);
		mav.setViewName("redirect:/process");
		return mav;
	}
	
}
