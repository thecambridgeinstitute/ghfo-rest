package com.ciie.ghfo.dao.impl;

import java.util.List;

import javax.annotation.Resource;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.ModificationItem;

import org.springframework.ldap.core.AttributesMapper;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.stereotype.Repository;

import com.ciie.ghfo.dao.UserDao;
import com.ciie.ghfo.rest.api.user.form.UserDetails;

@Repository("userDao")
public class UserDaoImpl implements UserDao {

	@Resource
	private LdapTemplate ldapTemplate;
	
	@SuppressWarnings({ "rawtypes" })
	private class UserAttributesMapper implements AttributesMapper{
		public Object mapFromAttributes(Attributes attrs) throws javax.naming.NamingException {
			UserDetails user = new UserDetails();
			user.setfName((String)attrs.get("cn").get());
			user.setlName((String)attrs.get("sn").get());
			user.seteMail((String)attrs.get("mail").get());
			user.setUid((String)attrs.get("uid").get());
			return user;
		}
	}
	
	@Override
	public boolean create(UserDetails userDetails) {
		try {
	        // 基类设置
			BasicAttribute ocattr = new BasicAttribute("objectClass");
			ocattr.add("top");
			ocattr.add("person");
			ocattr.add("uidObject");
			ocattr.add("inetOrgPerson");
			ocattr.add("organizationalPerson");
			// 用户属性
			Attributes attrs = new BasicAttributes();
			attrs.put(ocattr);
			attrs.put("cn", userDetails.getfName());
			attrs.put("sn", userDetails.getlName());
			attrs.put("mail",userDetails.geteMail());
			attrs.put("userPassword", userDetails.getPassoword());
			ldapTemplate.bind("uid=" + userDetails.getUid().trim(), null, attrs);
			return true;
	    } catch (Exception ex) {
			ex.printStackTrace();
			return false;
	    }
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public List<UserDetails> findUser(String userId) {
		String filter = "(&(objectclass=inetOrgPerson)(uid=" + userId + "))";
		return ldapTemplate.search("",filter,new UserAttributesMapper());
	}

	@SuppressWarnings({ "unchecked" })
	@Override
	public List<UserDetails> getAllUser() {
		return ldapTemplate.search("", "(objectclass=person)",  new UserAttributesMapper()
                );
	}


	@Override
	public boolean delete(String userId) {
		try {
			ldapTemplate.unbind("uid=" + userId);
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}

	public boolean updateUser(UserDetails userDetails) {
	    try {
	    	ldapTemplate.modifyAttributes("uid=" + userDetails.getUid().trim(), new ModificationItem[] {
		    new ModificationItem(DirContext.REPLACE_ATTRIBUTE, new BasicAttribute("cn", userDetails.getfName().trim())),
		    new ModificationItem(DirContext.REPLACE_ATTRIBUTE, new BasicAttribute("sn", userDetails.getlName().trim())),
		    new ModificationItem(DirContext.REPLACE_ATTRIBUTE, new BasicAttribute("mail", userDetails.geteMail().trim())),
		});
		return true;
	    } catch (Exception ex) {
		ex.printStackTrace();
		return false;
	    }
	}
	
}
