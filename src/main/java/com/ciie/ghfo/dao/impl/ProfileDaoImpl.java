package com.ciie.ghfo.dao.impl;

import org.springframework.stereotype.Repository;

import com.ciie.ghfo.commons.dao.impl.CommonDao;
import com.ciie.ghfo.dao.ProfileDao;

@Repository("profileDao")
public class ProfileDaoImpl extends CommonDao implements ProfileDao {

}
