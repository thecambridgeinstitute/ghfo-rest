package com.ciie.ghfo.dao;

import java.util.List;

import com.ciie.ghfo.rest.api.user.form.UserDetails;

public interface UserDao {
	
	public boolean create(UserDetails userDetails);
	
	public List findUser(String userId);
	
	public List<UserDetails> getAllUser();
	
	public boolean delete(String userId);
	
	public boolean updateUser(UserDetails userDetails);
}
