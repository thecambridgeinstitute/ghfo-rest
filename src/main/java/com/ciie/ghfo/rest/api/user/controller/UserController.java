package com.ciie.ghfo.rest.api.user.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.activiti.engine.impl.persistence.entity.UserEntity;
import org.activiti.ldap.LDAPConfigurator;
import org.activiti.ldap.LDAPUserManager;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.ciie.ghfo.ldap.activiti.UserManagerLdap;
import com.ciie.ghfo.rest.api.user.form.UserDetails;
import com.ciie.ghfo.services.UserService;

@RestController
@RequestMapping(value="/user")
public class UserController {
	
	private Logger logger = Logger.getLogger("UserController");
	
	@Resource
	private LDAPConfigurator ldapConfigurator;
	
	@Resource
	private UserService userService;
	
	@RequestMapping(method = RequestMethod.GET,value="/{userId}",headers="Accept=application/json")
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody UserEntity getUser(HttpServletRequest request, HttpServletResponse response,@PathVariable("userId")String userId){
		response.setHeader("Access-Control-Allow-Origin", "*");
		LDAPUserManager user = new LDAPUserManager(ldapConfigurator);
		logger.info(user.toString());
		
		return user.findUserById(userId);
	}
	
	@RequestMapping(method = RequestMethod.GET,value="/find/{userId}",headers="Accept=application/json")
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody List find(HttpServletRequest request, HttpServletResponse response,@PathVariable("userId")String userId){
		response.setHeader("Access-Control-Allow-Origin", "*");
		
		//logger.info(userDetails.toString());
		return userService.findUserById(userId);
	}
	
	@RequestMapping(method = RequestMethod.GET,value="/findAll",headers="Accept=application/json")
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody List<UserDetails> find(HttpServletRequest request, HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		List<UserDetails> list = userService.getAllUser();
		return list;
	}
	
}
