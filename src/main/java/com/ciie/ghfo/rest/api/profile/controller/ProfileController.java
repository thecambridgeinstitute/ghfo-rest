package com.ciie.ghfo.rest.api.profile.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.activiti.engine.runtime.ProcessInstance;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.ciie.ghfo.entity.ProFileEntity;
import com.ciie.ghfo.rest.api.profile.form.ProfileForm;
import com.ciie.ghfo.services.ProfileService;
import com.ciie.ghfo.services.WorkflowService;

/**
 * profile
 * @author Administrator
 *
 */
@RestController
@RequestMapping("/profile")
public class ProfileController {

	Logger logger = Logger.getLogger(ProfileController.class);
	
	@Autowired
	private ProfileService profileService;
	
	@Autowired
	private WorkflowService workflowService;

	/**
	 *  create profile
	 * @param profileForm
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST,value="/createprofile",headers="Accept=application/json")
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody String createProfile(HttpServletRequest request, HttpServletResponse response,RedirectAttributes redirectAttributes, ProfileForm profileForm){
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Headers", "X-Requested-With");
		ProFileEntity newpro = profileService.save(new ProFileEntity(profileForm));
		try{
			Map<String, Object> variables = new HashMap<String, Object>();
			variables.put("fName", newpro.getfName());
			variables.put("lName", newpro.getlName());
			variables.put("email", newpro.geteMail());
            ProcessInstance processInstance = workflowService.startWorkflow(newpro, variables);
			logger.info("new profile:"+newpro.toString());
			redirectAttributes.addFlashAttribute("Info:", "profile flow start，process id：" + processInstance.getId());
			return newpro.toString();
		}catch(Exception e){
			logger.info("add profile fail:"+e.getMessage());
			profileService.deleteObject(newpro);
			return "";
		}
	}
	
	/**
	 * load All profile
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET,value="/profiles",headers="Accept=application/json")
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody List<ProfileForm> list(HttpServletRequest request, HttpServletResponse response) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Headers", "X-Requested-With");
		try{
			List<ProFileEntity> list = profileService.listAll(ProFileEntity.class);
			List<ProfileForm> tempList = new ArrayList<ProfileForm>();
			for(ProFileEntity pro:list){
				tempList.add(new ProfileForm(pro));
			}
			logger.info("load profiles.....");
			return tempList;
		}catch(Exception e){
			logger.info("load profiles fail : "+e.getMessage());
			return null;
		}
	}
	
	/**
	 * get profile by id
	 * @param id
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET,value = "/{id}",headers="Accept=application/json")
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody ProfileForm getProfile(HttpServletRequest request, HttpServletResponse response, @PathVariable("id")Long id){
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Headers", "X-Requested-With");
		try{
			ProFileEntity pro = profileService.get(ProFileEntity.class, id);
			logger.info("get profile:"+pro.toString());
			return new ProfileForm(pro);
		}catch(Exception e){
			logger.info("load profile fail : "+e.getMessage());
			return null;
		}
	}
	
}
