package com.ciie.ghfo.rest.api.profile;

import org.activiti.rest.common.api.SecuredResource;
import org.activiti.rest.service.api.legacy.identity.LegacyStateResponse;
import org.restlet.resource.Put;

import com.ciie.ghfo.rest.api.profile.form.ProfileForm;

/**
 * 创建profile
 * @author Administrator
 *
 */
public class CreateProfileResource extends SecuredResource{

	@Put
	public LegacyStateResponse createProfile(ProfileForm profileForm){
		
		return new LegacyStateResponse().setSuccess(true);
		
	}
	
}
