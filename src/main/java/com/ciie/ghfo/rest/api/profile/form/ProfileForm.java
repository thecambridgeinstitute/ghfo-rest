package com.ciie.ghfo.rest.api.profile.form;

import java.io.Serializable;
import java.util.Date;

import com.ciie.ghfo.entity.ProFileEntity;

public class ProfileForm implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private long id;
	
	private String fName;
	
	private String lName;
	
	private Date dateBirth;
	
	private String gender;
	
	private Date applicationDate;
	
	private String phone;
	
	private String eMail;
	
	private String street;
	
	private String city;
	
	private String state;
	
	private Integer zipCode;
	
	private String source;

	public ProfileForm(){
		
	}
	
	public long getId() {
		return id;
	}



	public void setId(long id) {
		this.id = id;
	}



	public ProfileForm(String fName,String lName){
		this.fName=fName;
		this.lName=lName;
	}

	public String getfName() {
		return fName;
	}

	public void setfName(String fName) {
		this.fName = fName;
	}

	public String getlName() {
		return lName;
	}

	public void setlName(String lName) {
		this.lName = lName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Date getApplicationDate() {
		return applicationDate;
	}

	public void setApplicationDate(Date applicationDate) {
		this.applicationDate = applicationDate;
	}

	public String geteMail() {
		return eMail;
	}

	public void seteMail(String eMail) {
		this.eMail = eMail;
	}

	public Date getDateBirth() {
		return dateBirth;
	}

	public void setDateBirth(Date dateBirth) {
		this.dateBirth = dateBirth;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Integer getZipCode() {
		return zipCode;
	}

	public void setZipCode(Integer zipCode) {
		this.zipCode = zipCode;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public ProfileForm(ProFileEntity proFileEntity) {
		id = proFileEntity.getId();
		fName = proFileEntity.getfName();
		lName = proFileEntity.getlName();
		dateBirth = proFileEntity.getDateBirth();
		gender = proFileEntity.getGender();
		applicationDate = proFileEntity.getApplicationDate();
		phone = proFileEntity.getPhone();
		eMail = proFileEntity.geteMail();
		street = proFileEntity.getStreet();
		city = proFileEntity.getCity();
		state = proFileEntity.getState();
		zipCode = proFileEntity.getZipCode();
		source = proFileEntity.getSource();
	}

	@Override
	public String toString() {
		return "ProfileForm [fName=" + fName + ", lName=" + lName
				+ ", dateBirth=" + dateBirth + ", gender=" + gender
				+ ", applicationDate=" + applicationDate + ", phone=" + phone
				+ ", eMail=" + eMail + ", street=" + street + ", city=" + city
				+ ", state=" + state + ", zipCode=" + zipCode + ", source="
				+ source + "]";
	}
	
}
