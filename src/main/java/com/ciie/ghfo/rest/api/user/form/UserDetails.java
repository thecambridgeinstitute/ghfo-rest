package com.ciie.ghfo.rest.api.user.form;

import java.io.Serializable;

public class UserDetails implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String uid;
	
	private String fName;
	
	private String lName;
	
	private String eMail;
	
	private String passoword;

	public String getfName() {
		return fName;
	}

	public void setfName(String fName) {
		this.fName = fName;
	}

	public String getlName() {
		return lName;
	}

	public void setlName(String lName) {
		this.lName = lName;
	}

	public String geteMail() {
		return eMail;
	}

	public void seteMail(String eMail) {
		this.eMail = eMail;
	}

	public String getPassoword() {
		return passoword;
	}

	public void setPassoword(String passoword) {
		this.passoword = passoword;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	@Override
	public String toString() {
		return "UserDetails [uid=" + uid + ", fName=" + fName + ", lName="
				+ lName + ", eMail=" + eMail + ", passoword=" + passoword + "]";
	}
	
}
