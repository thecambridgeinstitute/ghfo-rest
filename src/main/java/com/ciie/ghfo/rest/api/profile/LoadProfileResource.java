package com.ciie.ghfo.rest.api.profile;

import org.activiti.rest.common.api.SecuredResource;
import org.restlet.data.Status;
import org.restlet.resource.Get;

import com.ciie.ghfo.rest.api.profile.form.ProfileForm;

public class LoadProfileResource extends SecuredResource {

	@Get
	public ProfileForm getProfile() {
		if (authenticate() == false)
			return null;

		return new ProfileForm();
	}

	protected Status getAuthenticationFailureStatus() {
		return Status.CLIENT_ERROR_FORBIDDEN;
	}
}
