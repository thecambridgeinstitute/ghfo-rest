<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ include file="../../commons/inc.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style type="text/css">
table,table td,table th {
	border: 1px solid gray;
	border-collapse: collapse;
}

a {
	height: 30px;
	line-height: 30px;
	border: 1px solid black;
	background: gray;
	color: white;
	text-decoration: none;
	padding: 3px;
	font-weight: bold;
}
</style>
</head>
<body>
	<%=(String) session.getAttribute("username")%>
	<div style="margin: 0 auto; width: 300px; padding-top: 50px;">
		<center><h2>流程模板</h2></center>
		<table width="1000;">
			<thead>
				<tr>
					<th>流程定义</th>
					<th>操作</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="temp" items="${list}">
					<tr>
						<td>${temp.name }</td>
						<td><a href="${ctx }/process/deployModel?modelId=${temp.id }">部署</a> 
							<a href="${ctx }/modeler/service/editor?id=${temp.id }">编辑</a>
							<a href="${ctx }/process/deleteModel?modelId=${temp.id }">删除</a>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table><br>
		<a href="${ctx }/process">已部署流程</a>
		<a href="${ctx }/process/started">已启动流程</a>
		<a href="${ctx }/process/task">任务列表</a>
	</div>
</body>
</html>