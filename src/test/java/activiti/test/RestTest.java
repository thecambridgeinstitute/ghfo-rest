package activiti.test;

import junit.framework.TestCase;

import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.ciie.ghfo.commons.utils.SpringContextUtil;
import com.ciie.ghfo.rest.api.profile.form.ProfileForm;




public class RestTest extends TestCase{
	
	private static RestTemplate restTemplate  = SpringContextUtil.getBean("restTemplate");
	
	@Test
	public void testCreateProfile(){
		 ProfileForm pro = new ProfileForm();
		 pro.setfName("joe");
		 pro.setlName("yong");
		 pro.setPhone("12318044295");
		 pro.setCity("chengdu");
		 HttpEntity<ProfileForm> he = new HttpEntity<ProfileForm>(pro);
		 ResponseEntity<ProfileForm> response = restTemplate.postForEntity("http://localhost:8080/ghfo-rest/profile/createprofile",he,ProfileForm.class);
		 ProfileForm e = response.getBody();
		 System.out.println(e.toString());
	}
	
}
